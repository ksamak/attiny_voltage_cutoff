EESchema Schematic File Version 4
LIBS:attiny_voltage_cutoff_12V-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATtiny:ATtiny85-20PU U?
U 1 1 5E84CAC4
P 3800 2850
F 0 "U?" H 3270 2896 50  0000 R CNN
F 1 "ATtiny85-20PU" H 3270 2805 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 3800 2850 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 3800 2850 50  0001 C CNN
	1    3800 2850
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5E84D5DF
P 1100 2750
F 0 "#PWR?" H 1100 2600 50  0001 C CNN
F 1 "+BATT" H 1115 2923 50  0000 C CNN
F 2 "" H 1100 2750 50  0001 C CNN
F 3 "" H 1100 2750 50  0001 C CNN
	1    1100 2750
	1    0    0    -1  
$EndComp
$Comp
L power:-BATT #PWR?
U 1 1 5E84D667
P 1100 3150
F 0 "#PWR?" H 1100 3000 50  0001 C CNN
F 1 "-BATT" H 1115 3323 50  0000 C CNN
F 2 "" H 1100 3150 50  0001 C CNN
F 3 "" H 1100 3150 50  0001 C CNN
	1    1100 3150
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:NXE2S1205MC optoelectric_12V->5V
U 1 1 5E84D8A5
P 2150 2950
F 0 "optoelectric_12V->5V" H 2150 3417 50  0000 C CNN
F 1 "B1205S-1W" H 2150 3326 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_muRata_NXE2SxxxxMC_THT" H 2150 2600 50  0001 C CNN
F 3 "http://power.murata.com/data/power/ncl/kdc_nxe2.pdf" H 2150 2450 50  0001 C CNN
	1    2150 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 3150 1450 3150
$Comp
L Connector:Screw_Terminal_01x02 Out+
U 1 1 5E84E427
P 5700 3600
F 0 "Out+" H 5779 3592 50  0000 L CNN
F 1 "Out-" H 5779 3501 50  0000 L CNN
F 2 "" H 5700 3600 50  0001 C CNN
F 3 "~" H 5700 3600 50  0001 C CNN
	1    5700 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3150 5500 3600
Wire Wire Line
	1450 3150 1450 3750
Wire Wire Line
	1450 3750 3000 3750
Connection ~ 1450 3150
Wire Wire Line
	1450 3150 1650 3150
Wire Wire Line
	2650 2750 2650 2250
Wire Wire Line
	2650 2250 3800 2250
Wire Wire Line
	2650 3150 2650 3450
Wire Wire Line
	2650 3450 3000 3450
Wire Wire Line
	3000 3450 3000 3750
Connection ~ 3000 3450
Wire Wire Line
	3000 3450 3800 3450
Wire Wire Line
	3000 3750 4700 3750
Wire Wire Line
	5500 3750 5500 3700
Connection ~ 3000 3750
$Comp
L Device:R 100Kohm
U 1 1 5E851D72
P 5000 2100
F 0 "100Kohm" H 5070 2146 50  0000 L CNN
F 1 "R" H 5070 2055 50  0000 L CNN
F 2 "" V 4930 2100 50  0001 C CNN
F 3 "~" H 5000 2100 50  0001 C CNN
	1    5000 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R 10Kohm
U 1 1 5E85210A
P 5000 2400
F 0 "10Kohm" H 5070 2446 50  0000 L CNN
F 1 "R" H 5070 2355 50  0000 L CNN
F 2 "" V 4930 2400 50  0001 C CNN
F 3 "~" H 5000 2400 50  0001 C CNN
	1    5000 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 1950 5500 1950
Wire Wire Line
	1100 2750 1450 2750
Wire Wire Line
	1450 2750 1450 1950
Wire Wire Line
	1450 1950 5000 1950
Connection ~ 1450 2750
Wire Wire Line
	1450 2750 1650 2750
Connection ~ 5000 1950
$Comp
L Transistor_FET:BSF030NE2LQ mosfet
U 1 1 5E84EDE0
P 5400 2950
F 0 "mosfet" H 5606 2996 50  0000 L CNN
F 1 "N-channel" H 5606 2905 50  0000 L CNN
F 2 "Package_DirectFET:DirectFET_SQ" H 5400 2950 50  0001 C CIN
F 3 "https://www.infineon.com/dgdl/Infineon-BSF030NE2LQ-DS-v02_03-en.pdf?fileId=db3a30432e398416012e47a8f0792588" H 5400 2950 50  0001 L CNN
	1    5400 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1950 5500 2750
Wire Wire Line
	5500 3750 5000 3750
Connection ~ 5000 3750
Wire Wire Line
	5000 2550 5000 2850
$Comp
L Device:CP 22uf
U 1 1 5E8A0368
P 4700 2700
F 0 "22uf" H 4818 2746 50  0000 L CNN
F 1 "CP" H 4818 2655 50  0000 L CNN
F 2 "" H 4738 2550 50  0001 C CNN
F 3 "~" H 4700 2700 50  0001 C CNN
	1    4700 2700
	1    0    0    -1  
$EndComp
Connection ~ 5000 2250
Wire Wire Line
	4700 2850 5000 2850
Wire Wire Line
	4550 2250 4700 2250
Connection ~ 5000 2850
Wire Wire Line
	5000 2850 5000 3750
Wire Wire Line
	4700 2250 4700 2550
Connection ~ 4700 2250
Wire Wire Line
	4700 2250 5000 2250
Wire Wire Line
	5200 2950 4700 2950
Wire Wire Line
	4500 2950 4500 2550
Wire Wire Line
	4500 2550 4400 2550
Wire Wire Line
	4550 2250 4550 2850
Wire Wire Line
	4550 2850 4400 2850
$Comp
L Device:R 10K
U 1 1 5E8C3293
P 4700 3350
F 0 "10K" H 4770 3396 50  0000 L CNN
F 1 "R" H 4770 3305 50  0000 L CNN
F 2 "" V 4630 3350 50  0001 C CNN
F 3 "~" H 4700 3350 50  0001 C CNN
	1    4700 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 2950 4700 3200
Connection ~ 4700 2950
Wire Wire Line
	4700 2950 4500 2950
Wire Wire Line
	4700 3500 4700 3750
Connection ~ 4700 3750
Wire Wire Line
	4700 3750 5000 3750
$EndSCHEMATC
