/*
   Ksamak's little voltage cutoff
   designed to fit lead-acid, target voltage range is 10V-16V
   material:
    - N-channel mosfet
    - resistor divider to manage input voltage measures
    - opto isolator B1205S-1W
*/

/*
 * TODO:
 * - put 10K on rst pin to gnd
 * - 5v voltage regulator
 */

//#define DEBUG
//#define DEBUG2 // MORE!

//#define BATT_VOLTAGE_DIVIDER_RATIO 11.14
#define BATT_VOLTAGE_DIVIDER_RATIO 10.64 // corrected for my test wrecked ATTINY85
#define VOL_RATIO BATT_VOLTAGE_DIVIDER_RATIO * 2.56 / 1023.0
#define VOLTAGE_MEASURE_PRECISION 0.30

#define VOLT_PIN PB3
#define MOSFET_PIN PB0

#ifdef DEBUG2
// debug timings, to try out the application.
#define VOLTAGE_CUTOFF 13.60
#define VOLTAGE_RESTORE 14.30
#define FLOAT_VOLTAGE 13.8
#define TIMING_CONSISTENT_VOLTAGE 5 // seconds till we consider the voltage consistent
#define TIMING_LOOP_OFF 1 // loop time when off
#define TIMING_LOOP_ON 2 // loop time when on
#define TIMING_JUST_OFF 5 // anti rebound timing, when just turned off
#define TIMING_FLOAT_DETECTION 30 // anti rebound timing, when just turned off

#else
// Normal timings
#define VOLTAGE_CUTOFF 13.20
#define VOLTAGE_RESTORE 14.30
#define FLOAT_VOLTAGE 13.3
#define TIMING_CONSISTENT_VOLTAGE 30 // seconds till we consider the voltage consistent
#define TIMING_LOOP_OFF 10 // loop time when off
#define TIMING_LOOP_ON 3 // loop time when on
#define TIMING_JUST_OFF 120 // anti rebound timing, when just turned off
#define TIMING_FLOAT_DETECTION 60 // anti rebound timing, when just turned off

#endif

#include <SoftwareSerial.h>

enum class Phase { on, off, stable_vol_wait }; // enum class

Phase phase;
SoftwareSerial serial(PB1, PB2);
float voltage;
bool just_off;
uint8_t float_counter;

void measure_voltage() {
  voltage = 0;
    for(uint8_t i=0; i<2; i++){
      delay(300);
      voltage += analogRead(VOLT_PIN);
    }
  voltage = voltage / 2. * VOL_RATIO;
}

void power_on() {
    digitalWrite(MOSFET_PIN, HIGH);
}

void power_off() {
    digitalWrite(MOSFET_PIN, LOW);
    just_off = true;
}

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  serial.begin(9600);
  pinMode(VOLT_PIN, INPUT);
  pinMode(MOSFET_PIN, OUTPUT);
  digitalWrite(MOSFET_PIN, LOW);
  analogReference(INTERNAL2V56);
  //analogReference(DEFAULT);

  phase = Phase::off;
  voltage = 0;
  float_counter = 0;
  just_off = false;

  serial.println();
  serial.println(F("booting up."));
  serial.println(F("letting 4s for capacitor to charge up"));
  delay(4000); // let capacitor charge
  serial.println(F("aaand we're phase::off"));
}

// the loop routine runs over and over again forever:
void loop() {
  switch(phase) {
    case Phase::off : // When off, monitor until voltage comes back up.
      delay(TIMING_LOOP_OFF * 1000);
      if (just_off) {
        just_off = false;
        delay(TIMING_JUST_OFF * 1000);
      }
      measure_voltage();
      #ifdef DEBUG2
      serial.print(F("phase: off.  voltage (voltage: "));
      serial.print(voltage);
      serial.println(F("V)"));
      #endif
      if (voltage >= VOLTAGE_RESTORE) {
        phase = Phase::stable_vol_wait;
        #ifdef DEBUG
        serial.print(F("switching phase: voltage stabilization (voltage: "));
        serial.print(voltage);
        serial.println(F("V)"));
        #endif
      }
      if (voltage <= FLOAT_VOLTAGE + VOLTAGE_MEASURE_PRECISION && voltage >= FLOAT_VOLTAGE - VOLTAGE_MEASURE_PRECISION) {
        float_counter++;
      } else {
        float_counter = 0;
      }
      if (float_counter > TIMING_FLOAT_DETECTION / TIMING_LOOP_OFF ) { // if we've been at 13.3 for TIMING_FLOAT_DETECTION
        float_counter = 0;
        phase = Phase::on;
        power_on();
        #ifdef DEBUG
        serial.print(F("switching phase: ON"));
        serial.print(F("cause: detected float voltage (voltage: "));
        serial.print(voltage);
        serial.println(F("V)"));
        #endif
      }
    break;

    case Phase::stable_vol_wait: // waiting until voltage is stable.
      for (uint8_t i=0; i<TIMING_CONSISTENT_VOLTAGE; i++) {
          delay(1000);
          measure_voltage();
          #ifdef DEBUG2
          serial.print(F("phase: stable_vol_wait. voltage (voltage: "));
          serial.print(voltage);
          serial.println(F("V)"));
          #endif
          if (voltage < VOLTAGE_RESTORE) {
            phase = Phase::off;

            #ifdef DEBUG
            serial.println(F("switching back phase: OFF"));
            serial.print(F("cause: voltage dropped below VOLTAGE_RESTORE (voltage: "));
            serial.print(voltage);
            serial.println(F("V)"));
            #endif
            break;
          }
      }
      phase = Phase::on;
      power_on();

      #ifdef DEBUG
      serial.print(F("switching phase: ON (voltage: "));
      serial.print(voltage);
      serial.println(F("V)"));
      #endif
    break;

    case Phase::on: // When on, monitor closely that voltage doesn't drop
      delay(TIMING_LOOP_ON * 1000);
      measure_voltage();
      #ifdef DEBUG2
      serial.print(F("phase: on. voltage (voltage: "));
      serial.print(voltage);
      serial.println(F("V)"));
      #endif
      if (voltage < VOLTAGE_CUTOFF) {
        phase = Phase::off;
        power_off();

        #ifdef DEBUG
        serial.println(F("switching to phase: OFF"));
        serial.print(F("cause: voltage dropped below VOLTAGE_CUTOFF (voltage: "));
        serial.print(voltage);
        serial.println(F("V)"));
        #endif
      }
    break;
  }
}
